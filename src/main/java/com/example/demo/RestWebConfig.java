import org.springframework.web.servlet.config.annotation.CorsRegistry;

public class RestWebConfig {   
    public void addCorsMappings(String corsAllowedOrigin, CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedMethods("GET", "POST", "PUT", "DELETE", "OPTIONS", "PATCH")
                .allowedOrigins(corsAllowedOrigin.split(","));
    }
}

